package ru.chelnokov.musicInstruments;

import ru.chelnokov.interfaces.Wind;

public class Flute implements Wind {
    @Override
    public void windInstruments() {
        System.out.println("Играет с помощью воздушных потоков");
    }
}
