package ru.chelnokov.musicInstruments;

import ru.chelnokov.interfaces.Keyboard;
import ru.chelnokov.interfaces.Wind;

public class Organ implements Keyboard, Wind {
    @Override
    public void keyboardInstruments() {
        System.out.println("Играет с помощью клавиш");
    }

    @Override
    public void windInstruments() {
        System.out.println("Играет с помощью воздушных потоков");
    }
}
