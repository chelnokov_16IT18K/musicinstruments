package ru.chelnokov.musicInstruments;

public class Main {
    public static void main(String[] args) {
        Piano piano = new Piano();
        piano.keyboardInstruments();
        piano.stringInstruments();

        Organ organ = new Organ();
        organ.keyboardInstruments();
        organ.windInstruments();
    }

}
