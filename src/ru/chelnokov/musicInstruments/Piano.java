package ru.chelnokov.musicInstruments;

import ru.chelnokov.interfaces.Keyboard;
import  ru.chelnokov.interfaces.String;

public class Piano implements Keyboard, String {
    @Override
    public void keyboardInstruments() {
        System.out.println("Играет с помощью клавиш");
    }

    @Override
    public void stringInstruments() {
        System.out.println("Играет с помощью струн");
    }
}
