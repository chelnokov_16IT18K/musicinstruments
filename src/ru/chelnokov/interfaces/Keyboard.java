package ru.chelnokov.interfaces;

/**
 * Интерфейс для клавишных музыкальных инструментов
 */

public interface Keyboard {
    void keyboardInstruments();
}
