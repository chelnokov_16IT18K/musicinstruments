package ru.chelnokov.interfaces;

/**
 * Интерфейс для духовых музыкальных инструментов
 */

public interface Wind {
    void windInstruments();
}
